package test.com.br.desafio

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import test.com.br.desafio.Adapter.ListaAdapter
import test.com.br.desafio.Api.RetrofitClient
import test.com.br.desafio.Model.ItemsLista
import test.com.br.desafio.Model.Lista

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ListaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Menu"

        progressBar.visibility = View.VISIBLE
        carregarLista()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cart -> {

                val intent = Intent(this@MainActivity, CartListActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun carregarLista() {
        val call = RetrofitClient().listaService().lista()
        call.enqueue(object : Callback<Lista> {
            override fun onResponse(
                call: Call<Lista>?,
                response: Response<Lista>?
            ) {
                response?.body()?.let {
                    val lista: Lista = it
                    configurarListagem(lista.products)
                }
            }

            override fun onFailure(call: Call<Lista>?, t: Throwable?) {
                progressBar.visibility = View.GONE
                alertDialog()
            }
        })
    }

    private fun configurarListagem(lista: List<ItemsLista>) {

        val recyclerView = recyclerView_list
        adapter = ListaAdapter(this)

        recyclerView.adapter = adapter

        adapter.setList(lista as ArrayList<ItemsLista>)

        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
        progressBar.visibility = View.GONE

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {}
        })

        adapter.setOnItemClickListener(object : ListaAdapter.ClickListener {
            override fun onClick(pos: Int, aView: View) {
                val intent = Intent(this@MainActivity, CartActivity::class.java);
                intent.putExtra("title", lista[pos].title)
                intent.putExtra("image", lista[pos].image)
                intent.putExtra("size", lista[pos].size)
                intent.putExtra("sugar", lista[pos].sugar)
                if (lista[pos].additional != null) {
                    intent.putParcelableArrayListExtra(
                        "data",
                        lista[pos].additional as java.util.ArrayList<out Parcelable>
                    )
                }
                startActivity(intent)
            }
        })

    }

    fun alertDialog() {

        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Atenção")

        builder.setMessage("Desculpe, não conseguimos concluir a requisição, deseja tentar novamente?")

        builder.setPositiveButton("Sim") { dialog, which ->
            val intent = Intent(this@MainActivity, MainActivity::class.java);
            startActivity(intent)
        }


        builder.setNegativeButton("Sair") { dialog, which ->

        }

        val dialog: AlertDialog = builder.create()

        dialog.show()
    }
}

private fun Any.putExtra(s: String, additional: List<String>) {

}
