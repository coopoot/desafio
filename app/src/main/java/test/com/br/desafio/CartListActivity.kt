package test.com.br.desafio

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_cart_list.*
import test.com.br.desafio.Adapter.ListaCarrinhoAdapter
import test.com.br.desafio.Model.ListaCarrinho

class CartListActivity : AppCompatActivity() {


    private lateinit var adapter: ListaCarrinhoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart_list)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = "Carrinho"

        if (Constants.listCart.isEmpty()) {
            tvMensagemCarrinho.visibility = View.VISIBLE
        } else {
            this.configurarListagem(Constants.listCart as ArrayList<ListaCarrinho>)
        }
    }

    private fun configurarListagem(lista: ArrayList<ListaCarrinho>) {

        val recyclerView = listaCarrinhoRecycle
        adapter = ListaCarrinhoAdapter(this)

        recyclerView.adapter = adapter

        adapter.setList(lista)

        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {}
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
