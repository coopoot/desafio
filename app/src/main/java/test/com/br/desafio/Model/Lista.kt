package test.com.br.desafio.Model

data class Lista(
    val products: List<ItemsLista>
)

data class ItemsLista(
    val title: String,
    val image: String,
    val size: Int,
    val sugar: Int,
    val additional: List<String>
)