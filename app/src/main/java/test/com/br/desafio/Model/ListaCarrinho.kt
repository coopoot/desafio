package test.com.br.desafio.Model

data class ListaCarrinho(
    val produto: String,
    val quantidade: Int,
    val tamanho: Int,
    val adicionais: String,
    val imagem: String
)