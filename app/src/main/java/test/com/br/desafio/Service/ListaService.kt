package test.com.br.desafio.Service

import retrofit2.Call
import retrofit2.http.GET
import test.com.br.desafio.Model.Lista

interface ListaService {

    @GET("products.json")
    fun lista(): Call<Lista>

}