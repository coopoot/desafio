package test.com.br.desafio.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_lista.view.*
import test.com.br.desafio.Model.ItemsLista
import test.com.br.desafio.R

class ListaAdapter(
    private val context: Context
) : Adapter<ListaAdapter.ViewHolder>() {

    private var lista: ArrayList<ItemsLista> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_lista, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lista = lista[position]

        holder.let {
            it.bindView(lista)
        }
    }

    fun setList(listaSet: ArrayList<ItemsLista>) {
        lista = listaSet
        notifyDataSetChanged()
    }

    fun addData(listaAdd: List<ItemsLista>) {
        this.lista.addAll(listaAdd)
        notifyDataSetChanged()
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        override fun onClick(v: View) {
            mClickListener.onClick(adapterPosition, v)
        }

        init {
            itemView.setOnClickListener(this)
        }

        fun bindView(lista: ItemsLista) {
            val title = itemView.item_title
            val image = itemView.item_image

            title.text = lista.title

            Glide.with(itemView.context)
                .load(lista.image)
                .into(image)
        }
    }

}