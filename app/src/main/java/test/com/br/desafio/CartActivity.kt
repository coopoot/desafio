package test.com.br.desafio

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_cart.*
import test.com.br.desafio.Model.ListaCarrinho

class CartActivity : AppCompatActivity() {

    private var adicionalAtivo: Boolean = false
    private var title: String? = null
    private var image: String? = null
    private var size: Int = 0
    private var sugar: Int = 0
    var list: ArrayList<Parcelable> = ArrayList()


    var quantidade: Int = 0
    var tamanho: Int = 0
    var acucar: Int = 0
    var adicional: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = "Detalhes"

        try {
            title = intent.getStringExtra("title")
            image = intent.getStringExtra("image")
            size = intent.getIntExtra("size", size)
            sugar = intent.getIntExtra("sugar", sugar)

            list = intent.getParcelableArrayListExtra("data")

        } catch (e: Exception) {
        }

        this.tamanhoCafe()
        this.quantidadeAcucar()
        this.quantidade()

        if (list.size > 0) {
            this.adicional()
        } else {
            adicional1.visibility = View.INVISIBLE
        }

        btnAddCarrinho.setOnClickListener {

            //            Constants.listCart.toMutableList(ListaCarrinho(title.toString(), quantidade, tamanho, adicional, image.toString()))
            Constants.listCart.add(ListaCarrinho(title.toString(), quantidade, tamanho, adicional, image.toString()))

            val builder = AlertDialog.Builder(this@CartActivity)
            builder.setTitle("Sucesso")

            builder.setMessage("Adicionamos $title ao seu carrinho")


            builder.setNegativeButton("Fechar") { dialog, which ->
                val intent = Intent(this@CartActivity, MainActivity::class.java)
                startActivity(intent)
            }

            val dialog: AlertDialog = builder.create()

            dialog.show()
        }
    }

    fun tamanhoCafe() {

        size1.setOnClickListener {
            this.visibleItemSize(1)
        }
        size2.setOnClickListener {
            this.visibleItemSize(2)
        }
        size3.setOnClickListener {
            this.visibleItemSize(3)
        }
    }

    fun adicional() {
        adicional1.setOnClickListener {
            if (adicionalAtivo) {
                adicional1.alpha = 0.5F
            } else {
                adicional1.alpha = 1.0F
                adicional = list[0].toString()
            }
        }
    }

    fun quantidadeAcucar() {

        sugar0.setOnClickListener {
            this.visibleAcucarSize(0)
        }
        sugar1.setOnClickListener {
            this.visibleAcucarSize(1)
        }
        sugar2.setOnClickListener {
            this.visibleAcucarSize(2)
        }
        sugar3.setOnClickListener {
            this.visibleAcucarSize(3)
        }
    }

    fun quantidade() {
        btnQuantidadeMais.setOnClickListener {
            quantidade++
            tvQuantidade.text = quantidade.toString()
        }
        btnQuantidadeMenos.setOnClickListener {
            if (quantidade > 0) {
                quantidade--
            }
            tvQuantidade.text = quantidade.toString()
        }
    }

    fun visibleItemSize(size: Int) {
        when (size) {
            1 -> {
                size1.alpha = 1.0F
                size2.alpha = 0.5F
                size3.alpha = 0.5F
                tamanho = 1
            }
            2 -> {
                size1.alpha = 0.5F
                size2.alpha = 1.0F
                size3.alpha = 0.5F
                tamanho = 2
            }
            3 -> {
                size1.alpha = 0.5F
                size2.alpha = 0.5F
                size3.alpha = 1.0F
                tamanho = 3
            }
        }
    }

    fun visibleAcucarSize(size: Int) {
        when (size) {
            0 -> {
                sugar0.alpha = 1.0F
                sugar1.alpha = 0.5F
                sugar2.alpha = 0.5F
                sugar3.alpha = 0.5F
                acucar = 0
            }
            1 -> {
                sugar0.alpha = 0.5F
                sugar1.alpha = 1.0F
                sugar2.alpha = 0.5F
                sugar3.alpha = 0.5F
                acucar = 1
            }
            2 -> {
                sugar0.alpha = 0.5F
                sugar1.alpha = 0.5F
                sugar2.alpha = 1.0F
                sugar3.alpha = 0.5F
                acucar = 2
            }
            3 -> {
                sugar0.alpha = 0.5F
                sugar1.alpha = 0.5F
                sugar2.alpha = 0.5F
                sugar3.alpha = 1.0F
                acucar = 3
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}

private fun <E> List<E>.toMutableList(listaCarrinho: E) {

}
