package test.com.br.desafio.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_lista_carrinho.view.*
import test.com.br.desafio.Model.ListaCarrinho
import test.com.br.desafio.R

class ListaCarrinhoAdapter(
    private val context: Context
) : Adapter<ListaCarrinhoAdapter.ViewHolder>() {

    private var lista: ArrayList<ListaCarrinho> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_lista_carrinho, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lista = lista[position]

        holder.let {
            it.bindView(lista)
        }
    }

    fun setList(listaSet: ArrayList<ListaCarrinho>) {
        lista = listaSet
        notifyDataSetChanged()
    }

    fun addData(listaAdd: List<ListaCarrinho>) {
        this.lista.addAll(listaAdd)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(lista: ListaCarrinho) {
            val title = itemView.tvProdutoCarrinho
            val image = itemView.item_image_carrinho
            val quantidade = itemView.tvQuantidadeCarrinho
            val adicionais = itemView.tvAdicionaisCarrinho

            title.text = lista.produto
            quantidade.text = lista.quantidade.toString()
            adicionais.text = lista.adicionais

            Glide.with(itemView.context)
                .load(lista.imagem)
                .into(image)
        }
    }

}