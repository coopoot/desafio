package test.com.br.desafio.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.com.br.desafio.Service.ListaService

class RetrofitClient {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://desafio-mobility.herokuapp.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun listaService() = retrofit.create(ListaService::class.java)
}